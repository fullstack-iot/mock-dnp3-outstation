mod utils;
use std::error::Error;
use utils::{load_config, Level};

#[tokio::main(flavor = "multi_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    let config = load_config().unwrap();
    tracing_subscriber::fmt()
        .with_max_level(match config.log_level {
            Level::ERROR => tracing::Level::ERROR,
            Level::WARN => tracing::Level::WARN,
            Level::INFO => tracing::Level::INFO,
            Level::DEBUG => tracing::Level::DEBUG,
        })
        .with_target(false)
        .init();
    tracing::info!("Running with: {:?}", config);
    Ok(())
}
