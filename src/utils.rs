use serde::{Deserialize, Serialize};
use std::env;
use std::net::Ipv4Addr;
use validator::Validate;

#[derive(Debug, PartialEq, Deserialize, Serialize)]
#[allow(clippy::upper_case_acronyms)]
pub enum Level {
    ERROR,
    WARN,
    INFO,
    DEBUG,
}

#[derive(Validate, Debug, Deserialize, Serialize)]
pub struct Config {
    pub log_level: Level,
    #[validate(range(min = 80, max = 65535))]
    pub port: u16,
    pub host: Ipv4Addr,
}

#[derive(Debug, Deserialize, Serialize)]
struct ConfigMap {
    development: Config,
    staging: Config,
}

pub fn load_config() -> Result<Config, ()> {
    let f = std::fs::File::open("cfg.yml").expect("Could not open file.");
    let config_map: ConfigMap = serde_yaml::from_reader(f).expect("Could not read values.");
    let environment = env::var("ENV").unwrap_or_else(|_| "development".to_string());
    let config = match environment.as_str() {
        "staging" => config_map.staging,
        _ => config_map.development,
    };
    match config.validate() {
        Ok(_) => (),
        Err(e) => panic!("{}", e),
    };
    Ok(config)
}
