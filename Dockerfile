FROM rust:1.75.0-slim-bookworm
WORKDIR /app
COPY . .
RUN cargo install --path .
CMD mock-dnp3-outstation