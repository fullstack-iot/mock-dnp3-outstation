# Mock DNP3 Outstation 🦀 

![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-dnp3-outstation?branch=00-scaffolding&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/badge/rust-1.75.0-gray?style=for-the-badge&logo=rust)


## Pre-Requisites
 ```shell
cargo install cargo-cmd \
               cargo-audit \
               cargo-udeps --locked \
               cargo2junit \
               cargo-watch 
```
